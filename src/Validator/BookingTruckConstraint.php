<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
#[\Attribute]
class BookingTruckConstraint extends Constraint
{
    public string $message = 'This foodtruck has already booked in the week';

    public function getTargets()
    {
        return self::PROPERTY_CONSTRAINT;
    }
}
